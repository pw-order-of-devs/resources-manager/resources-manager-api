package api

import (
	"github.com/gin-gonic/gin"
	"resources-manager-api/src/api/v1"
)

func V1(router *gin.Engine) {
	version1 := router.Group("/api/v1")
	{
		v1.Resources(version1)
	}
}

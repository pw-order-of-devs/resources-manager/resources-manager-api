package storage

import (
	"fmt"
	"github.com/gofrs/uuid"
	"resources-manager-api/src/model/pagination"
	mr "resources-manager-api/src/model/resources"
	ms "resources-manager-api/src/model/storage"
	"resources-manager-api/src/storage/impl/awsS3Bucket"
	"resources-manager-api/src/storage/impl/mariadb"
	"resources-manager-api/src/storage/impl/memory"
	"resources-manager-api/src/storage/impl/mysql"
	"resources-manager-api/src/storage/impl/postgres"
	"resources-manager-api/src/utils"
	"sync"
)

type Provider interface {
	GetProviderType() ms.ProviderType
	GetAll(p pagination.Pagination) (mr.PaginatedResource, error)
	Get(id string) (*mr.Resource, error)
	GetContent(id string) ([]byte, error)
	UploadContent(id string, content []byte) (int, error)
	Create(entry mr.FileMetadata) (*uuid.UUID, error)
	Update(id string, entry mr.FileMetadata) (int, error)
	Del(id string) (int, error)
}

var lock = &sync.Mutex{}
var provider Provider

func getInstance() Provider {
	lock.Lock()
	defer lock.Unlock()
	if provider == nil {
		utils.Logger.Debug("Creating Provider instance")
		switch utils.GetProviderType() {
		case ms.AwsS3Bucket:
			provider = awsS3Bucket.NewProvider()
		case ms.MariaDb:
			provider = mariadb.NewProvider()
		case ms.Mysql:
			provider = mysql.NewProvider()
		case ms.Postgres:
			provider = postgres.NewProvider()
		default:
			provider = new(memory.Provider)
		}
		utils.Logger.Debug(fmt.Sprintf("Created Provider instance of type %s", provider.GetProviderType()))
	} else {
		utils.Logger.Debug(fmt.Sprintf("Provider instance already exists with type %s", provider.GetProviderType()))
	}
	return provider
}

func GetProvider() Provider {
	return getInstance()
}

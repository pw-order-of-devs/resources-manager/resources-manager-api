package mysql

import (
	ms "resources-manager-api/src/model/storage"
	"resources-manager-api/src/storage/impl/shared"
)

type Provider struct {
	shared.DefaultProvider
}

func (mp Provider) GetProviderType() ms.ProviderType {
	return ms.Mysql
}

func NewProvider() *Provider {
	return &Provider{shared.DefaultProvider{
		GetAllQuery:                getAllQuery,
		GetResourceQuery:           getResourceQuery,
		GetResourceContentQuery:    getResourceContentQuery,
		UploadResourceContentQuery: uploadResourceContentQuery,
		CreateResourceQuery:        createResourceQuery,
		UpdateResourceQuery:        updateResourceQuery,
		DeleteResourceQuery:        deleteResourceQuery,
	}}
}

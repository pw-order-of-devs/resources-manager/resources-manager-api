package awsS3Bucket

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/feature/dynamodb/attributevalue"
	"github.com/aws/aws-sdk-go-v2/feature/s3/manager"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb/types"
	"github.com/aws/aws-sdk-go-v2/service/s3"
	"github.com/aws/smithy-go"
	"github.com/gofrs/uuid"
	ha "resources-manager-api/src/helpers/cloud/aws"
	"resources-manager-api/src/model/pagination"
	mr "resources-manager-api/src/model/resources"
	ms "resources-manager-api/src/model/storage"
	"resources-manager-api/src/utils"
	"sort"
	"strconv"
	"time"
)

type Provider struct {
	bucketName        string
	dynamodbTableName string
}

func (mp Provider) GetProviderType() ms.ProviderType {
	return ms.AwsS3Bucket
}

func NewProvider() *Provider {
	return &Provider{
		bucketName:        utils.GetEnv("AWS_BUCKET_NAME"),
		dynamodbTableName: utils.GetEnv("AWS_DYNAMODB_TABLE"),
	}
}

func parseResource(item map[string]types.AttributeValue) *mr.Resource {
	var entry mr.FileMetadata
	var id string
	_ = attributevalue.Unmarshal(item["id"], &id)
	var creationDate int64
	_ = attributevalue.Unmarshal(item["create_date"], &creationDate)
	var modificationDate *int64
	_ = attributevalue.Unmarshal(item["modification_date"], &modificationDate)
	if err := attributevalue.UnmarshalMap(item, &entry); err == nil {
		return &mr.Resource{
			Id:               utils.GetUuidFromString(id),
			CreationDate:     creationDate,
			ModificationDate: modificationDate,
			Metadata:         entry,
		}
	} else {
		return nil
	}
}

func (mp Provider) GetAll(p pagination.Pagination) (mr.PaginatedResource, error) {
	if items, err := ha.GetDynamoDbClient().Scan(
		context.TODO(),
		&dynamodb.ScanInput{TableName: aws.String(mp.dynamodbTableName)},
	); err != nil {
		return mr.PaginatedResource{Pagination: p.WithTotal(-1)}, err
	} else {
		resources := make([]mr.Resource, 0)
		for _, item := range items.Items {
			resource := parseResource(item)
			if resource != nil {
				resources = append(resources, *resource)
			}
		}
		sort.Slice(resources, func(i, j int) bool { return resources[i].Id.String() < resources[j].Id.String() })
		total := len(resources)
		pages := p.WithTotal(total)
		start, end := pagination.GetPageBounds(p, total)
		if start >= len(resources) {
			return mr.PaginatedResource{Pagination: pages}, nil
		} else {
			return mr.PaginatedResource{Resources: resources[start:end], Pagination: pages}, nil
		}
	}
}

func (mp Provider) Get(id string) (*mr.Resource, error) {
	key := make(map[string]types.AttributeValue)
	key["id"] = &types.AttributeValueMemberS{Value: id}
	if item, err := ha.GetDynamoDbClient().GetItem(
		context.TODO(),
		&dynamodb.GetItemInput{
			TableName: aws.String(mp.dynamodbTableName),
			Key:       key,
		},
	); err != nil {
		return nil, err
	} else if item.Item["id"] == nil {
		return nil, utils.NoSuchElementError
	} else {
		return parseResource(item.Item), nil
	}
}

func (mp Provider) GetContent(id string) ([]byte, error) {
	buff := &manager.WriteAtBuffer{}
	if _, err := manager.NewDownloader(ha.GetAWSClient()).Download(
		context.TODO(),
		buff,
		&s3.GetObjectInput{
			Bucket: aws.String(mp.bucketName),
			Key:    aws.String(id),
		},
	); err != nil {
		var apiErr smithy.APIError
		if errors.As(err, &apiErr) && apiErr.ErrorCode() == "NoSuchKey" {
			return nil, utils.NoSuchElementError
		}
		return nil, err
	} else {
		return buff.Bytes(), nil
	}
}

func (mp Provider) UploadContent(id string, content []byte) (int, error) {
	if _, err := manager.NewUploader(ha.GetAWSClient()).Upload(
		context.TODO(),
		&s3.PutObjectInput{
			Body:   bytes.NewReader(content),
			Bucket: aws.String(mp.bucketName),
			Key:    aws.String(id),
		},
	); err != nil {
		return 0, err
	} else {
		return 1, nil
	}
}

func (mp Provider) Create(metadata mr.FileMetadata) (*uuid.UUID, error) {
	uid := utils.GetUuid()
	if item, err := attributevalue.MarshalMap(metadata); err != nil {
		return nil, err
	} else {
		item["id"] = &types.AttributeValueMemberS{Value: uid.String()}
		item["create_date"] = &types.AttributeValueMemberN{Value: strconv.FormatInt(time.Now().UnixMilli(), 10)}
		item["modification_date"] = &types.AttributeValueMemberNULL{Value: true}
		if _, err := ha.GetDynamoDbClient().PutItem(
			context.TODO(),
			&dynamodb.PutItemInput{
				TableName: aws.String(mp.dynamodbTableName),
				Item:      item,
			},
		); err != nil {
			return nil, err
		} else {
			return uid, nil
		}
	}
}

func (mp Provider) Update(id string, metadata mr.FileMetadata) (int, error) {
	if element, err := mp.Get(id); err != nil {
		return 0, fmt.Errorf("element was not found")
	} else {
		if item, err := attributevalue.MarshalMap(metadata); err != nil {
			return 0, err
		} else {
			item["id"] = &types.AttributeValueMemberS{Value: id}
			item["create_date"] = &types.AttributeValueMemberN{Value: strconv.FormatInt(element.CreationDate, 10)}
			item["modification_date"] = &types.AttributeValueMemberN{Value: strconv.FormatInt(time.Now().UnixMilli(), 10)}
			if _, err := ha.GetDynamoDbClient().PutItem(
				context.TODO(),
				&dynamodb.PutItemInput{
					TableName: aws.String(mp.dynamodbTableName),
					Item:      item,
				},
			); err != nil {
				return 0, err
			} else {
				return 1, nil
			}
		}
	}
}

func (mp Provider) Del(id string) (int, error) {
	key := make(map[string]types.AttributeValue)
	key["id"] = &types.AttributeValueMemberS{Value: id}
	if _, err := ha.GetDynamoDbClient().DeleteItem(
		context.TODO(),
		&dynamodb.DeleteItemInput{
			TableName: aws.String(mp.dynamodbTableName),
			Key:       key,
		},
	); err != nil {
		return 0, err
	} else {
		return 1, nil
	}
}

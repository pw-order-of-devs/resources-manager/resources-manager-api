package memory

import (
	"github.com/gofrs/uuid"
	"resources-manager-api/src/model/pagination"
	mr "resources-manager-api/src/model/resources"
	ms "resources-manager-api/src/model/storage"
	"resources-manager-api/src/utils"
	"sort"
	"time"
)

var inMemoryResources = make([]mr.Resource, 0)
var inMemoryResourcesContent = make(map[string][]byte, 0)

type Provider struct{}

func (mp Provider) GetProviderType() ms.ProviderType {
	return ms.InMemory
}

func (mp Provider) GetAll(p pagination.Pagination) (mr.PaginatedResource, error) {
	total := len(inMemoryResources)
	pages := p.WithTotal(total)
	start, end := pagination.GetPageBounds(p, total)
	if start >= len(inMemoryResources) {
		return mr.PaginatedResource{Pagination: pages}, nil
	} else {
		return mr.PaginatedResource{Resources: inMemoryResources[start:end], Pagination: pages}, nil
	}
}

func (mp Provider) Get(id string) (*mr.Resource, error) {
	if _, resource := findResourceById(id); resource == nil {
		return nil, utils.NoSuchElementError
	} else {
		return resource, nil
	}
}

func (mp Provider) GetContent(id string) ([]byte, error) {
	if _, resource := findResourceById(id); resource == nil {
		return nil, utils.NoSuchElementError
	} else {
		return inMemoryResourcesContent[id], nil
	}
}

func (mp Provider) UploadContent(id string, content []byte) (int, error) {
	if _, res := findResourceById(id); res == nil {
		return 0, nil
	} else {
		inMemoryResourcesContent[id] = content
		return 1, nil
	}
}

func (mp Provider) Create(metadata mr.FileMetadata) (*uuid.UUID, error) {
	uid := utils.GetUuid()
	resource := mr.Resource{
		Id:           uid,
		CreationDate: time.Now().UTC().UnixMilli(),
		Metadata:     metadata,
	}
	inMemoryResources = append(inMemoryResources, resource)
	return uid, nil
}

func (mp Provider) Update(id string, metadata mr.FileMetadata) (int, error) {
	if idx, res := findResourceById(id); res == nil {
		return 0, nil
	} else {
		timeNow := time.Now().UTC().UnixMilli()
		inMemoryResources[idx].Metadata = metadata
		inMemoryResources[idx].ModificationDate = &timeNow
		return 1, nil
	}
}

func (mp Provider) Del(id string) (int, error) {
	if idx, res := findResourceById(id); res == nil {
		return 0, nil
	} else {
		removeResource(idx)
		return 1, nil
	}
}

func findResourceById(id string) (int, *mr.Resource) {
	sort.Slice(inMemoryResources, func(i, j int) bool {
		return inMemoryResources[i].Id.String() <= inMemoryResources[j].Id.String()
	})
	idx := sort.Search(len(inMemoryResources), func(i int) bool {
		return inMemoryResources[i].Id.String() == id
	})
	if idx < len(inMemoryResources) && inMemoryResources[idx].Id.String() == id {
		return idx, &inMemoryResources[idx]
	} else {
		return -1, nil
	}
}

func removeResource(idx int) {
	resource := inMemoryResources[idx]
	inMemoryResources = append(inMemoryResources[:idx], inMemoryResources[idx+1:]...)
	delete(inMemoryResourcesContent, resource.Id.String())
}

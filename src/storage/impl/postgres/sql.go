package postgres

const getAllQuery = `
select id, modification_date, creation_date, file_type, name,
	description, author_id, size, duration, tags, time_pinpoints
from resource_provider.resources
limit $1 offset $2
`

const getResourceQuery = `
select id, modification_date, creation_date, file_type, name,
    description, author_id, size, duration, tags, time_pinpoints
from resource_provider.resources
where id = $1
`

const getResourceContentQuery = `
select content
from resource_provider.resources
where id = $1
`

const uploadResourceContentQuery = `
update resource_provider.resources
set content = $1
where id = $2
`

const createResourceQuery = `
insert into resource_provider.resources (
	id, content, creation_date, file_type,
	name, description, author_id, size, duration, tags, time_pinpoints
) values ($1, '', $2, $3, $4, $5, $6, $7, $8, $9, $10)
`

const updateResourceQuery = `
update resource_provider.resources
set modification_date = $1, file_type = $2, name = $3, description = $4,
	author_id = $5, size = $6, duration = $7, tags = $8, time_pinpoints = $9
where id = $10
`

const deleteResourceQuery = `
delete
from resource_provider.resources
where id = $1
`

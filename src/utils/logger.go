package utils

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"
	"os"
)

func logLevel() zapcore.Level {
	if level, err := zapcore.ParseLevel(GetEnvOrDefault("LOG_LEVEL", "INFO")); err != nil {
		return zapcore.InfoLevel
	} else {
		return level
	}
}

var jsonEncoder = zapcore.NewJSONEncoder(zap.NewProductionEncoderConfig())
var textEncoder = zapcore.NewConsoleEncoder(zap.NewProductionEncoderConfig())

var fileLogger = zapcore.NewCore(
	jsonEncoder,
	zapcore.AddSync(&lumberjack.Logger{
		Filename:   GetEnvOrDefault("LOGS_PATH", "/var/log/resources-manager/resources-manager-api.log"),
		MaxSize:    GetIntEnvOrDefault("LOGS_MAX_SIZE", 500),
		MaxBackups: GetIntEnvOrDefault("LOGS_MAX_BACKUPS", 7),
		MaxAge:     GetIntEnvOrDefault("LOGS_MAX_AGE", 28),
	}),
	logLevel(),
)

var consoleLogger = zapcore.NewCore(
	textEncoder,
	zapcore.AddSync(os.Stdout),
	logLevel(),
)

func zapOpts() []zap.Option {
	return []zap.Option{zap.AddCaller()}
}

func loggers() []zapcore.Core {
	cores := make([]zapcore.Core, 0)
	cores = append(cores, consoleLogger)
	if GetBoolEnvOrDefault("LOGS_FILE_ENABLED", true) {
		cores = append(cores, fileLogger)
	}
	return cores
}

var Logger = zap.New(zapcore.NewTee(loggers()...), zapOpts()...)

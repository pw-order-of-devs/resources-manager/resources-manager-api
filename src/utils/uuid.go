package utils

import "github.com/gofrs/uuid"

func GetUuid() *uuid.UUID {
	if id, err := uuid.DefaultGenerator.NewV4(); err != nil {
		return nil
	} else {
		return &id
	}
}

func GetUuidFromString(uid string) *uuid.UUID {
	if id, err := uuid.FromString(uid); err != nil {
		return nil
	} else {
		return &id
	}
}

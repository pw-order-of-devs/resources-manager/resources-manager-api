package postgres

import (
	"fmt"
	"resources-manager-api/src/utils"
)

type DatabaseClient struct{}

func (DatabaseClient) ConnString() string {
	return fmt.Sprintf(
		"host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		utils.GetEnvOrDefault("POSTGRES_HOST", "localhost"),
		utils.GetIntEnvOrDefault("POSTGRES_PORT", 5432),
		utils.GetEnv("POSTGRES_USER"),
		utils.GetEnv("POSTGRES_PASSWORD"),
		utils.GetEnv("POSTGRES_DB"),
	)
}

func (DatabaseClient) Driver() string {
	return "postgres"
}

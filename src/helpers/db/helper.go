package db

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	_ "github.com/lib/pq"
	"resources-manager-api/src/helpers/db/mariadb"
	"resources-manager-api/src/helpers/db/mysql"
	"resources-manager-api/src/helpers/db/postgres"
	ms "resources-manager-api/src/model/storage"
	"resources-manager-api/src/utils"
	"sync"
)

type DatabaseClient interface {
	ConnString() string
	Driver() string
}

var lock = &sync.Mutex{}
var clientInstance *sql.DB

func getInstance(provider ms.ProviderType) *sql.DB {
	lock.Lock()
	defer lock.Unlock()
	if clientInstance == nil {
		utils.Logger.Debug(fmt.Sprintf("Creating %s client instance", provider))
		var client DatabaseClient
		switch provider {
		case ms.MariaDb:
			client = new(mariadb.DatabaseClient)
		case ms.Mysql:
			client = new(mysql.DatabaseClient)
		case ms.Postgres:
			client = new(postgres.DatabaseClient)
		default:
			utils.FatalError(fmt.Errorf("not supported database client"))
		}
		if db, err := sql.Open(client.Driver(), client.ConnString()); err != nil {
			utils.FatalError(err)
		} else {
			db.SetMaxIdleConns(24)
			db.SetMaxOpenConns(24)
			clientInstance = db
		}
	} else {
		utils.Logger.Debug(fmt.Sprintf("%s client instance already exists", provider))
	}
	return clientInstance
}

func GetDbClient() *sql.DB {
	return getInstance(utils.GetProviderType())
}

package aws

import (
	"context"
	"fmt"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/credentials"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
	"github.com/aws/aws-sdk-go-v2/service/s3"
	"resources-manager-api/src/utils"
	"sync"
)

var lock = &sync.Mutex{}
var clientInstance *s3.Client
var dynamodbInstance *dynamodb.Client

func getConfig() aws.Config {
	cfg, err := config.LoadDefaultConfig(context.TODO(), func(opts *config.LoadOptions) error {
		opts.Region = utils.GetEnv("AWS_REGION")
		if len(utils.GetEnvOrDefault("AWS_PROFILE", "")) > 0 {
			opts.SharedConfigProfile = utils.GetEnv("AWS_PROFILE")
		} else if len(utils.GetEnvOrDefault("AWS_CRED_KEY", "")) > 0 {
			opts.Credentials = credentials.NewStaticCredentialsProvider(
				utils.GetEnv("AWS_CRED_KEY"),
				utils.GetEnv("AWS_CRED_SECRET"),
				utils.GetEnvOrDefault("AWS_CRED_SESSION", ""),
			)
		} else {
			utils.FatalError(fmt.Errorf("missing AWS authentication"))
		}
		return nil
	})
	if err != nil {
		utils.FatalError(err)
	}
	return cfg
}

func getClientInstance() *s3.Client {
	lock.Lock()
	defer lock.Unlock()
	if clientInstance == nil {
		utils.Logger.Debug("Creating AWS S3 client instance")
		clientInstance = s3.NewFromConfig(getConfig())
	} else {
		utils.Logger.Debug("AWS S3 client instance already exists")
	}
	return clientInstance
}

func GetAWSClient() *s3.Client {
	return getClientInstance()
}

func getDynamoDbInstance() *dynamodb.Client {
	lock.Lock()
	defer lock.Unlock()
	if dynamodbInstance == nil {
		utils.Logger.Debug("Creating AWS dynamoDB client instance")
		dynamodbInstance = dynamodb.NewFromConfig(getConfig())
	} else {
		utils.Logger.Debug("AWS dynamoDB client instance already exists")
	}
	return dynamodbInstance
}

func GetDynamoDbClient() *dynamodb.Client {
	return getDynamoDbInstance()
}

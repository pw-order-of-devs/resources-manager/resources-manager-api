package pagination

import (
	"github.com/gin-gonic/gin"
	"strconv"
)

type Pagination struct {
	PageNumber int
	PageSize   int
	Total      int
}

func (p Pagination) WithTotal(total int) Pagination {
	p.Total = total
	return p
}

func getQueryParam(c *gin.Context, key string, def int) int {
	if item := c.Query(key); len(item) == 0 {
		return def
	} else if itemInt, err := strconv.Atoi(item); err != nil {
		return def
	} else {
		if itemInt < 1 {
			return 1
		} else {
			return itemInt
		}
	}
}

func GetPagination(c *gin.Context) Pagination {
	return Pagination{
		PageNumber: getQueryParam(c, "pageNumber", 1),
		PageSize:   getQueryParam(c, "pageSize", 20),
	}
}

func GetPageBounds(p Pagination, max int) (int, int) {
	start := (p.PageNumber - 1) * p.PageSize
	end := p.PageNumber * p.PageSize
	if end >= max {
		end = max
	}
	return start, end
}

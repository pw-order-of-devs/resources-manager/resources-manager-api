package resources

import (
	"fmt"
	"github.com/gofrs/uuid"
	"github.com/lib/pq"
	"resources-manager-api/src/model/pagination"
	"strconv"
	"strings"
)

type FileType string

const (
	VIDEO FileType = "video"
	AUDIO FileType = "audio"
	TEXT  FileType = "text"
)

type TimePinpoint struct {
	Moment      int64
	Description string
}

func TimePinpointsToPqArray(tpa []TimePinpoint) interface{} {
	items := make([]string, len(tpa))
	for i, tp := range tpa {
		items[i] = fmt.Sprintf("(%d,%s)", tp.Moment, tp.Description)
	}
	return pq.Array(items)
}

func (tp *TimePinpoint) Scan(value interface{}) error {
	if sourceBytes, ok := value.([]uint8); !ok {
		return fmt.Errorf("incompatible type")
	} else {
		source := string(sourceBytes)
		for _, old := range []string{"\\\"", "\"", "{", "}", "(", ")"} {
			source = strings.ReplaceAll(source, old, "")
		}
		tpItemRawData := strings.Split(source, ",")
		moment, err := strconv.ParseInt(tpItemRawData[0], 10, 64)
		if err != nil {
			return fmt.Errorf("parse TimePinpoint raw data (%s) error: %v", source, err)
		}
		*tp = TimePinpoint{
			Moment:      moment,
			Description: tpItemRawData[1],
		}
		return nil
	}
}

type FileMetadata struct {
	FileType      *FileType
	Name          string
	Description   *string
	AuthorId      *string
	Size          *float64
	Duration      *float32
	TimePinpoints []TimePinpoint
	TagList       []string
}

type Resource struct {
	Id               *uuid.UUID
	CreationDate     int64
	ModificationDate *int64
	Metadata         FileMetadata
}

type PaginatedResource struct {
	Resources  []Resource
	Pagination pagination.Pagination
}

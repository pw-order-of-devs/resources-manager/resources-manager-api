build:

	docker-compose -f docker-compose-dev.yml build api

lint:

	golangci-lint run

up:

	docker-compose -f docker-compose-dev.yml up -d api

down:

	docker-compose -f docker-compose-dev.yml down
	docker rmi rm-api

logs:

	docker-compose -f docker-compose-dev.yml logs -f api

test-inmemory:

	docker-compose -f docker-compose-dev.yml up tests-inmemory

test-postgres:

	docker-compose -f docker-compose-dev.yml up tests-postgres

test-mariadb:

	docker-compose -f docker-compose-dev.yml up tests-mariadb

test-mysql:

	docker-compose -f docker-compose-dev.yml up tests-mysql

test-aws-s3:

	docker-compose -f docker-compose-dev.yml up tests-aws-s3

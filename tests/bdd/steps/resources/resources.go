package resources

import (
	"encoding/json"
	"fmt"
	"github.com/cucumber/godog"
	"github.com/gofrs/uuid"
	"resources-manager-api/src/model/pagination"
	mr "resources-manager-api/src/model/resources"
	sr "resources-manager-api/src/services/resources"
	ua "resources-manager-api/src/utils/api"
)

var resp ua.Resp
var resourceId *uuid.UUID

/// Resources API v1

func InitializeScenario(sc *godog.ScenarioContext) {
	/// Resources API v1
	sc.Step(`^Create resource request is received$`, createResourcesRequestIsReceived)
	sc.Step(`^Resource is created$`, resourceIsCreated)
	sc.Step(`^Upload content request is received$`, uploadContentRequestIsReceived)
	sc.Step(`^Resource content is uploaded$`, resourceContentIsUploaded)
	sc.Step(`^Update resource request is received$`, updateResourcesRequestIsReceived)
	sc.Step(`^Resource is updated$`, resourceIsUpdated)
	sc.Step(`^Get all resources request is received$`, getAllResourcesRequestIsReceived)
	sc.Step(`^List of all resources is returned$`, listOfAllResourcesIsReturned)
	sc.Step(`^Get resource by id request is received$`, getResourceByIdRequestIsReceived)
	sc.Step(`^Resource is returned$`, resourceIsReturned)
	sc.Step(`^Get resource content by id request is received$`, getResourceContentByIdRequestIsReceived)
	sc.Step(`^Resource content is not empty`, resourceContentIsNotEmpty)
	sc.Step(`^Delete resource request is received$`, deleteResourcesRequestIsReceived)
	sc.Step(`^Resource is deleted$`, resourceIsDeleted)
}

func createResourcesRequestIsReceived() {
	fileTypeText := mr.TEXT
	resource, _ := json.Marshal(mr.FileMetadata{
		FileType:      &fileTypeText,
		Name:          "file_name_1.txt",
		TagList:       []string{"tag1", "tag2"},
		TimePinpoints: []mr.TimePinpoint{{Moment: 0, Description: "moment 0"}},
	})
	resp = sr.Create(resource)
}

func resourceIsCreated() error {
	printError(resp.Err)
	if resp.HttpStatus != 201 {
		return fmt.Errorf("status code should be 201, was: %d", resp.HttpStatus)
	}
	data, _ := resp.Item.([]byte)
	if uid, err := uuid.FromString(string(data)); err != nil {
		return fmt.Errorf("expected uuid v4 as response")
	} else {
		resourceId = &uid
	}
	return nil
}

func uploadContentRequestIsReceived() {
	resp = sr.UploadContent(resourceId.String(), []byte("file content"))
}

func resourceContentIsUploaded() error {
	printError(resp.Err)
	if resp.HttpStatus != 200 {
		return fmt.Errorf("status code should be 200, was: %d", resp.HttpStatus)
	} else if string(resp.Item.([]byte)) != "1" {
		return fmt.Errorf("expected 1 modified record, was %s", string(resp.Item.([]byte)))
	}
	return nil
}

func updateResourcesRequestIsReceived() {
	fileTypeText := mr.TEXT
	resource, _ := json.Marshal(mr.FileMetadata{
		FileType:      &fileTypeText,
		Name:          "file_name_1.txt",
		TimePinpoints: []mr.TimePinpoint{{Moment: 0, Description: "moment 0"}},
	})
	resp = sr.Update(resourceId.String(), resource)
}

func resourceIsUpdated() error {
	printError(resp.Err)
	if resp.HttpStatus != 200 {
		return fmt.Errorf("status code should be 200, was: %d", resp.HttpStatus)
	} else if string(resp.Item.([]byte)) != "1" {
		return fmt.Errorf("expected 1 modified record, was %s", string(resp.Item.([]byte)))
	}
	return nil
}

func getAllResourcesRequestIsReceived() {
	resp = sr.GetAll(pagination.Pagination{PageNumber: 1, PageSize: 20})
}

func listOfAllResourcesIsReturned() error {
	printError(resp.Err)
	if resp.HttpStatus != 200 {
		return fmt.Errorf("status code should be 200, was: %d", resp.HttpStatus)
	} else if len(resp.Item.(mr.PaginatedResource).Resources) == 0 {
		return fmt.Errorf("expected non-empty list of entries")
	}
	return nil
}

func getResourceByIdRequestIsReceived() {
	resp = sr.Get(resourceId.String())
}

func resourceIsReturned() error {
	printError(resp.Err)
	if resp.HttpStatus != 200 {
		return fmt.Errorf("status code should be 200, was: %d", resp.HttpStatus)
	}
	return nil
}

func getResourceContentByIdRequestIsReceived() {
	resp = sr.GetContent(resourceId.String())
}

func resourceContentIsNotEmpty() error {
	printError(resp.Err)
	if resp.HttpStatus != 200 {
		return fmt.Errorf("status code should be 200, was: %d", resp.HttpStatus)
	} else if len(resp.Item.([]byte)) == 0 {
		return fmt.Errorf("downloaded content should not be empty")
	}
	return nil
}

func deleteResourcesRequestIsReceived() {
	resp = sr.Delete(resourceId.String())
}

func resourceIsDeleted() error {
	printError(resp.Err)
	if resp.HttpStatus != 200 {
		return fmt.Errorf("status code should be 200, was: %d", resp.HttpStatus)
	} else if string(resp.Item.([]byte)) != "1" {
		return fmt.Errorf("expected 1 deleted record, was %s", string(resp.Item.([]byte)))
	}
	return nil
}

func printError(err error) {
	if err != nil {
		fmt.Printf("%s", err.Error())
	}
}
